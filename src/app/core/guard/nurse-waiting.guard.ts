import { inject } from '@angular/core';
import { CanActivateFn } from '@angular/router';
import { NurseWaitingGuardService } from '../../shared/nurse-waiting-guard.service';

export const nurseWaitingGuard: CanActivateFn = (route, state) => {
  const authService = inject(NurseWaitingGuardService);
  return authService.isAllow();
};
