import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { LayoutService } from 'src/app/layout/service/app.layout.service';
import { HomeService } from 'src/app/backend/components/home/home.service';
import { Table } from 'primeng/table';
import * as _ from 'lodash';

@Component({
    selector: 'app-backend-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
    // breadcrumb items
    blockedPanel: boolean = false;
    home: any;
    homeData: any = [];
    loading: boolean = true;
    slot_type: any = [];
    all_data: any;
    all_data2: any[] = [];
    data_name: any[] = [];

    @ViewChild('filter') filter!: ElementRef;
    all_data1: any;
    constructor(
        private layoutService: LayoutService,
        private homeService: HomeService
    ) {
    }

    async ngOnInit() {

        await this.getData();
        this.loading = false;
    }

    // get Services data from API
    async getData() {

        this.blockedPanel = true;
        let hospitalID: any = sessionStorage.getItem('hospitalId');
        let serviceID: any = sessionStorage.getItem('serviceId');
        try {
            const res: any = await this.homeService.list(hospitalID, serviceID);
            let all_data = [];
            this.all_data2=[];
            all_data = res.data;
            console.log("---รีเทินจาก api ทั้งหมด", res);
            if (all_data.ok) {

                //this.all_data = all_data.results[0].reserve[0];
                //data_reserve.customer = [all_data.results[0].reserve[0].customer];

                //this.data_name = data_reserve.customer;

                for (let v of all_data.results) {
                    let for_v = v.reserve;

                  

                    for (let s of for_v) {
                        let for_s = s.customer;

                        this.data_name.push(for_s);

                    }

                    this.all_data2.push(for_v[0]);
                }
                console.log("ssssssss", this.data_name);
                console.log("ttttttttt", this.all_data2);
            } else {
                alert('Error loading DATA');
            }
            this.blockedPanel = false;
        } catch (error) {
            this.blockedPanel = false;
            console.log(error);
        }

    }


    onGlobalFilter(table: Table, event: Event) {

        table.filterGlobal((event.target as HTMLInputElement).value, 'contains');

    }

    clear(table: Table) {
        table.clear();
        this.filter.nativeElement.value = '';
    }

    scrollTo(viewChild: HTMLElement) {
        viewChild.scrollIntoView({ behavior: 'smooth' });
    }

    get backgroundStyle(): object {
        let path = 'assets/demo/images/landing/';
        let image = this.layoutService.config.colorScheme === 'dark' ? 'line-effect-dark.svg' : 'line-effect.svg';

        return { 'background-image': 'url(' + path + image + ')' };
    }

    get colorScheme(): string {
        return this.layoutService.config.colorScheme;
    }
}