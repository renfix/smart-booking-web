import { Component, ElementRef, ViewChild } from '@angular/core';
import { LayoutService } from 'src/app/layout/service/app.layout.service';
import { ManagementsService } from './managements.service';
import { LookupService} from '../../services/lookup.service';
import { FilterService, SelectItemGroup } from 'primeng/api';
import { Table } from 'primeng/table';

@Component({
    selector: 'app-backend-managements',
    templateUrl: './managements.component.html',
    styleUrls: ['./managements.component.scss']
})
export class ManagementsComponent {

    @ViewChild('filter') filter!: ElementRef;

    constructor(
        private layoutService: LayoutService, 
        private managementsService: ManagementsService,
        private lookupService: LookupService,
        private filterService: FilterService
        ) {}

    scrollTo(viewChild: HTMLElement) {
        viewChild.scrollIntoView({behavior: 'smooth'});
    }

    get backgroundStyle(): object {
        let path = 'assets/demo/images/landing/';
        let image = this.layoutService.config.colorScheme === 'dark' ? 'line-effect-dark.svg' : 'line-effect.svg'; 

        return {'background-image': 'url(' + path + image + ')'};
    }

    get colorScheme(): string {
        return this.layoutService.config.colorScheme;
    }

    management:any[] = [];
    listSlots:any;
    listPeriods:any;
    listSlotByID:any;
    slot_name:any;
    listServices:any;
    listUsers:any;
    listCustomers:any;
    displayForm: boolean = false;
    selectedCountryAdvanced: any[] = [];
    filteredCountries: any[] = [];
    countries: any[] = [];
    reserveID:any;

    managementName :string = '';
    selectedDrop:any;
    managementActive :boolean = true;

    managementIdEdit :number = 0;
    managementNameEdit :string = '';
    managementActiveEdit :boolean = true;

    isAdd :boolean = false;
    isEdit :boolean = false;




    managementStatus = [
        {label: 'Active', value: true},
        {label: 'Inactive', value: false}
    ];




    async ngOnInit() {
        await this.getService();
        
        await this.getPeriod();
   
        await this.getUser();
        await this.getData();
        
    }

    onGlobalFilter(table: Table, event: Event) {
        table.filterGlobal((event.target as HTMLInputElement).value, 'contains')
    }

    async getSlot() {
        const res:any = await this.lookupService.listSlot();
        let slot = [];
        slot = res.data;
        if(slot.ok){
            this.listSlots = slot.results;
        } else {
            alert('error load period')
        }
      
        
    }

    async getPeriod() {
        const res:any = await this.lookupService.listPeriod();
        let period = [];
        period = res.data;
        if(period.ok){
            this.listPeriods = period.results;
        } else {
            alert('error load period')
        }
       
        
    }

    async getService() {
        const res:any = await this.lookupService.listServiceType();
        let service = [];
        service = res.data;
        if(service.ok){
            this.listServices = service.results;
        } else {
            alert('error load period')
        }
        
        
    }

    async getUser() {
        const res:any = await this.lookupService.listUser();
        let user = [];
        user = res.data;
        if(user.ok){
            this.listUsers = user.results;
        } else {
            alert('error load period')
        }
    
        
    }

    async getCustomer(customer_id:any) {
        const res:any = await this.lookupService.listCustomerByID(customer_id);
        let customer = [];
        customer = res.data;
        if(customer.ok){
            this.listCustomers = customer.results[0];
        } else {
            alert('error load period')
        }
      
        
    }

    async getByServiceID(service_id:any) {
        const res:any = await this.lookupService.listSlotByID(service_id);
        let slot = [];
        slot = res.data;
        if(slot.ok){
            this.listSlotByID = slot.results;
            for(let v of  this.listSlotByID){ 
                v.valuename=v.slot_name+' วันที่ '+ this.thaiDateFormat(v.slot_date)+' ('+v.period_name+'น.)'
            }
            
        } else {
            alert('error load period')
        }
       
        
    }

    thaiDateFormat(date:any){
        const thaiMonth = [
            'ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.'
        ]
        const newDate = new Date(date);
        let day = newDate.getDate();
        let monthName = thaiMonth[newDate.getMonth()];
        let thaiYear = newDate.getFullYear() + 543;

        let thaidate = day + ' ' + monthName + ' ' + thaiYear;
        return thaidate;
        
    }

   
    
    



    // get Services data from API
    async getData() {
       const hospital:any= sessionStorage.getItem('hospitalId');
       const hospitalID: any = sessionStorage.getItem('hospitalId');
       const serviceID: any = sessionStorage.getItem('serviceId');
    
        console.log(hospital);
        console.log(serviceID);
        
        
        const day="2024-01-10";
        let info={'hospital_id':hospital,'reserve_date':day}
        const res:any  = await this.managementsService.list(hospitalID,serviceID);
        this.management=[];
        // let managements:any;
        let managements_:any = res.data;
       
        if(managements_.ok){
            for(let v of managements_.results){
                let managements = v.reserve;
                
                
                // for(let s of managements){
                //     let p:any = this.listSlots.find((p:any) => p.slot_id === s.slot_id);
                    
                //     s.slot_name = p.slot_name
                // }

                for(let s of managements){
                    let p:any = this.listPeriods.find((p:any) => p.period_id === s.period_id);
                   
                    s.period_name = p.period_name
                }
    
                for(let s of managements){
                    let p:any = this.listServices.find((p:any) => p.service_type_id === s.service_type_id);
                    s.service_type_name = p.service_type_name
                }
    
                for(let s of managements){
                    let p:any = this.listUsers.find((p:any) => p.user_id === s.user_id);
                    s.username = p.username
                }
    
                for(let s of managements){
    
                   await this.getCustomer(s.customer_id);
                //    let p:any = this.listCustomers.find((p:any) => p.customer_id === s.customer_id);
                    s.customer_name = this.listCustomers.customer_name
                    s.phone_number = this.listCustomers.phone_number
           
                    
                }

                for(let s of managements){
    
                    await this.getByServiceID(serviceID);
                     s.slot_name = this.listSlotByID.slot_name
            
                     
                 }
               
                this.management.push(managements[0]);
                console.log(this.management);
                
            }
    
    
            
        } else {
            alert('Error loading managements');
        }



    }

    // display form add data
    displayFormAdd() {
        this.isAdd = true;
        this.isEdit = false;
        this.displayForm = true;
        this.managementName = '';
    }

    // display form edit data
    displayFormEdit(data: any) {
       
        
        this.isAdd = false;
        this.isEdit = true;
        this.managementIdEdit = data.service_id;
        this.managementNameEdit = data.service_name;
        this.managementActiveEdit = data.is_active;
        this.displayForm = true;
        this.reserveID=data.reserve_id;
    }

    // function save data
    async save() {
        let data = {
            service_name: this.managementName
        };

        // save data
        await this.managementsService.save(data);

        // close form
        this.displayForm = false;
        
        //refresh data
        await this.getData();
    }

    // function update data
    async update() {
        
            let body = {
                slot_id: this.selectedDrop.slot_id,
            };
            
            // update data
            await this.managementsService.update(this.reserveID, body);
            
            
    
            // close form
            this.displayForm = false;
    
            // refresh data
            await this.getData();
       

        }

        async isconfirm(reserve:any,is_confirm:any) {
            
            
        if(is_confirm==false){

            let body = {
                is_confirm: true,
            };
            
            // update data
            await this.managementsService.update(reserve, body);
            
            
    
            // close form
            this.displayForm = false;
    
            // refresh data
            

        }else{
            let body = {
                is_confirm: false,
            };
            
            // update data
            await this.managementsService.update(reserve, body);
            
            
    
            // close form
            this.displayForm = false;
        }

        await this.getData();
            
       

        }
        
    

    // set is active = false
    async disable(id: number) {
        let body = {
            is_active: false
        };
        await this.managementsService.update(id, body);

        // refresh data
        await this.getData();
    }
    
    // set is active = true
    async enable(id: number) {
        let body = {
            is_active: true
        };
        await this.managementsService.update(id, body);

        // refresh data
        await this.getData();
    }

    
}