import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ActivitiesRoutingModule } from './activities-routing.module';
import { ActivitiesComponent } from './activities.component';
import { BlockUIModule } from 'primeng/blockui';
import { ButtonModule } from 'primeng/button';



@NgModule({
  declarations: [
    ActivitiesComponent
  ],
  imports: [
    CommonModule,
    ActivitiesRoutingModule,
    BlockUIModule,
    ButtonModule
  ]
})
export class ActivitiesModule { }
